# docker-one

Dockerfiles and docker-compose for opennebula. The base image is Ubuntu:18.04 and
Opennebula 5.6.

The are two Dockerfiles, one for the base `oned` deamon in directory `one` and another
for the opennebula sunstone `one-sunstone`.

The docker-compose.yaml relies on the existence of an `.env` file that has evironment variables.
Currently only the `oneadmin` password. You should create this `.env` file with the contents:

* `ONEPASS=<password>`

